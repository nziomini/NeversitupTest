package Permutation_test

import (
	"reflect"
	"testing"
)

func Test_Permutation(t *testing.T) {
	var _TextData = []struct {
		inputData  string
		ExpectData []string
	}{
		{"a", []string{"a"}},
		{"ab", []string{"ab", "ba"}},
		{"abc", []string{"abc", "acb", "bac", "bca", "cab", "cba"}},
		{"aabb", []string{"aabb", "abab", "abba", "baab", "baba", "bbaa"}},
	}

	for _, test := range _TextData {

		if !reflect.DeepEqual(Perm(test.inputData), test.ExpectData) {
			t.Errorf("Permutation(%q) = %v", test.inputData, Perm(test.inputData))
		}
		// fmt.Println("test = ", Perm(test.inputData))
		// t.Error("test")
	}
}

func Perm(str string) []string {

	var _result []string
	if len(str) == 1 {
		return []string{str}
	}
	for i := 0; i < len(str); i++ {
		_str := str[:i] + str[i+1:]
		for _, v := range Perm(_str) {
			if !CheckDup(_result, string(str[i])+v) {

				_result = append(_result, string(str[i])+v)
			}
		}

	}

	return _result
}
func CheckDup(s []string, e string) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}
