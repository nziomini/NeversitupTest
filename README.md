```
├── ...
├── main.go
├── domain                  # example auth
│   ├── endpoint.go                # กำหนด endpoint
│   ├── service.go                 # service ต่างๆ
│   ├── model.go                   # object model
│   ├── repository.go              # วางโครงสร้างเกียวกับการเชื่อมต่อไปยัง database เพื่อให้ service สามารถเรียกใช้ฟังชั่นต่างๆได้
│   ├── service_test.go            # test service
│   └── transport.go               # ทำหน้าที่ในการ routing และ middleware 
├── database                  # example auth
│   ├── auth.go               # จะทำหน้าที่เป็นตัวเชื่อมต่อกับ ฐานข้อมูล เช่่นการ get update create
└── ...
```
